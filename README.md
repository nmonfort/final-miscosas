# Entrega practica final MisCosas 2020

## Datos
* Nombre: Natalia Monforte Rodríguez
* Titulación: Doble Grado en Ingeniería en Sistemas de Telecomunicaciones + ADE
* Cuenta de laboratorio: nmonfort
* Despliegue (url): [URL despliegue]
* Video básico (url): [URL básico]
* Video parte opcional (url): [URL opcional]

[URL despliegue]: https://nataliamr7.pythonanywhere.com
[URL básico]: https://www.youtube.com/watch?v=vJDZRjkwCSY
[URL opcional]: https://www.youtube.com/watch?v=KzVPp40KaSU

## Cuenta Admin Site
* usuario/contraseña: admin/admin

## Cuentas usuarios
* usuario/contraseña: admin/admin
* usuario/contraseña: natalia/natalia
* usuario/contraseña: greta/greta

## Resumen parte obligatoria
Algunas peculiariades a tener en cuenta son las siguientes:
* La aplicación se debe ejecutar en https://nataliamr7.pythonanywhere.com para que se cargue correctamente el css. Si se lanza en otro servidor será necesario modificar las urls en las plantillas.
* Cuando un usuario se registra, se le añade una foto de perfil por defecto.
* El formulario que ofrece la página principal para seleccionar alimentador es único. Es decir, el mismo formulario permite seleccionar la fuente de alimentación y, a continuación, un alimentador para esa fuente. Si el usuario introduce un alimentador inválido, se devuelve una página de error indicando el error.
* El usuario puede elegir el estilo y tamaño de letra de su página por separado. Bastará con dejar uno de los campos del formulario vacío.
* Para la correcta gestión de las imágenes es necesaria la instalación del paquete de Python: Pillow (se ha dejado indicado en el fichero requirements.txt)
* En el recurso / se ofrece una lista de aplicaciones (actualmente sólo se encuentra la de MisCosas). Si se pincha en "MisCosas", se produce la redirección a la página principal de la web desplegada (https://nataliamr7.pythonanywhere.com/miscosas). Si se quisiera lanzar en otro servidor sería necesario modificar las urls de las plantillas HTML.
* El servidor servirá páginas indicando los errores que pudiera haber cometido un usuario al utilizar una página. Por ejemplo:
- Intento de registro con un nombre de usuario ya registrado.
- Login de un usuario sin cuenta o introducir una contraseña incorrecta.
- Acceder a la página de un item, alimentador o usuario que no exista.

## Lista partes opcionales
* Favicon del sitio.
* Visualización de cualquier página en formato JSON y XML.
* Generación de un canal RSS para los comentarios.
* Incorporación de alimentadores de la DGT.
* Atención al idioma indicado por el navegador (los idiomas soportados son es-ES y en-GB)
* Utilización de Bootstrap para la maquetación del sitio.
* Inclusión de imágenes en los comentarios. Se ha implementado permitiendo al usuario subir una imagen a la aplicación a través de un formulario. El usuario podrá comentar incluyendo o no incluyendo una imagen.
* Mejora de los test de la práctica. Además de los test que comprueban el código HTTP, se han implementado los siguientes test:
- Test para cada uno de los parser (Youtube, Flickr y DGT)
- Test para el almacenamiento de la información en la base de datos
- Test para comprobar si las vistas se sirven adecuadamente
- Test para comprobar si el HTML que devuelve la aplicación es el correcto
* Utilización de filtros de Python para conocer el voto que ha realizado un usuario a un item en el pasado y poder destacar el botón Like o Dislike correspondiente.
* Todos los formularios utilizados son formularios de Django (incluidos en el fichero forms.py).
