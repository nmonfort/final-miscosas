from django import template
from miscosas.models import Voto_Usuario

# Esta línea permite usar tags sin instalar una aplicación
register = template.Library()

# Creamos el filtro, para saber si un item ha sido votado por un usuario
@register.filter(name='vote_item')
def vote_item(item, username):
    return Voto_Usuario.objects.filter(usuario = username, item = item)
