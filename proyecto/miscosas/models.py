from django.db import models
from django.contrib.auth.models import User

# Create your models here.

#Fuentes: youtube, flickr y DGT
class FuenteAlimentador(models.Model):
    name = models.TextField() 
    def __str__(self):
        return self.name

#Alimentadores de la DGT: Noticias, Reportajes, Internacional, Opinión, Entrevistas, Motor
class DGT(models.Model):
    tipo = models.TextField()
    def __str__(self):
        return self.tipo

#Alimentador
class Alimentador(models.Model):
    source = models.TextField() #DGT, youtube o flickr
    alimentador = models.TextField() #id por el que buscamos en el form de la página principal
    name = models.TextField(default="") #Nombre del alimentador
    url = models.TextField(default="") #link del alimentador
    puntuation = models.IntegerField() #Puntuación global de los items
    count = models.IntegerField() #Número de items
    select = models.BooleanField(default=True) #Seleccionado o No Seleccionado

    def __str__(self):
        return self.alimentador
#Usuario
class Usuario(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE) #Usuario de ADMIN
    foto = models.ImageField(upload_to="miscosas/", default="/miscosas/perfil0.png") #Foto de perfil -> por defecto perfil0
    items = models.IntegerField(default=0) #Número de items votados
    comentarios = models.IntegerField(default=0) #Número de items comentados

    def __str__(self):
        return self.usuario.username
    
#Item
class Item(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE) #Alimentador al que pertenece
    item_name = models.TextField() #Nombre item
    item_link = models.TextField() #Link item
    item_id = models.TextField(default="") #Id para el recurso de la página del item
    thumbnail = models.TextField() #Thumbnail
    alimentador_link = models.TextField() #Url alimentador
    alimentador_name = models.TextField() #Nombre alimentador
    description = models.TextField(null=True) #Descripción item
    like = models.IntegerField() #Número de likes
    dislike = models.IntegerField() #Número de dislikes
    puntuation = models.IntegerField() #Puntuación total
    
    def __str__(self):
        return self.item_name
   
#Voto asociado a un usuario
class Voto_Usuario(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE) #Item votado
    usuario = models.ForeignKey(User, on_delete=models.CASCADE) #Usuario que vota
    like = models.BooleanField() # True -> LIKE False -> DISLIKE
    puntuation = models.IntegerField(default=0) #Puntuación total del item
    time = models.DateTimeField('votado') #Fecha de votación

    def __str__(self):
        return self.usuario.username

class Comentario(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE) #Item comentado
    usuario = models.ForeignKey(User, on_delete=models.CASCADE) #Usuario que comenta
    comentario = models.CharField(max_length=256) #Comentario
    foto = models.ImageField(upload_to="miscosas/") #Foto
    fecha = models.DateTimeField('publicado') #Fecha de comentario

    def __str__(self):
        return self.usuario.username
    
class TamanoLetra(models.Model):
    tamano = models.CharField(max_length=32, null=True) #Pequeño, mediano y grande

    def __str__(self):
        return self.tamano

class Estilo(models.Model):
    estilo = models.CharField(max_length=32, null=True) #Ligero y oscuro

    def __str__(self):
        return self.estilo

class CSS(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE) #CSS asociado a este usuario
    estilo = models.ForeignKey(Estilo, on_delete=models.CASCADE) #Estilo elegido
    tamaño = models.ForeignKey(TamanoLetra, on_delete=models.CASCADE) #Tamaño elegido
    def __str__(self):
        return ('CSS de ' + self.usuario.username)



