from django.test import TestCase, SimpleTestCase
from . import views
from django.contrib.auth.models import User
from .models import Alimentador
from .models import Item
from .models import Usuario
from .parseryt import YTParser
from .parserflickr import FlickrParser
from .parserdgt import DGTParser

# Create your tests here.

class TestYTParser(TestCase):
    # Inicialización de variables para los test del parser
    def setUp(self):
        self.simpleFile = 'miscosas/testdata/youtube.xml'
        self.zeroFile = 'miscosas/testdata/youtube-0.xml'
        self.oneFile = 'miscosas/testdata/youtube-1.xml'
        self.item_name = "Implementación de aplicaciones web: Counter WebApp"
        self.item_link = "https://www.youtube.com/watch?v=WlqYH7clQ0c"
        self.item_id = "WlqYH7clQ0c"
        self.thumbnail = "https://i4.ytimg.com/vi/WlqYH7clQ0c/hqdefault.jpg"
        self.alimentador_link = "https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg"
        self.alimentador_name = "CursosWeb"
        self.description = "Explicación de una aplicación web sencilla, counterapp.py, y de su estructura como ejemplo de la estructura de las aplicaciones web del lado del servidor en general."
        self.like = 0
        self.dislike = 0
        self.puntuation = 0

    # Test del parser con un xml de 1 sólo vídeo. Número de vídeos parseados + chequeo de los elementos parseados
    def test_parser_content(self):
        xmlFile = open(self.oneFile, 'r')
        YTParser.parse(xmlFile)
        video = Item.objects.get(item_id = "WlqYH7clQ0c") 
        videos = Item.objects.all() 
        self.assertEqual(len(videos), 1)
        self.assertEqual(video.item_link, self.item_link)
        self.assertEqual(video.item_id, self.item_id)
        self.assertEqual(video.thumbnail, self.thumbnail)
        self.assertEqual(video.alimentador_link, self.alimentador_link)
        self.assertEqual(video.alimentador_name, self.alimentador_name)
        self.assertEqual(video.description, self.description)
        self.assertEqual(video.like, self.like)
        self.assertEqual(video.dislike, self.dislike)
        self.assertEqual(video.puntuation, self.puntuation)

    # Test del parser con un xml de 6 vídeos
    def test_parser_count(self):
        xmlFile = open(self.simpleFile, 'r')
        YTParser.parse(xmlFile)
        videos = Item.objects.all() 
        self.assertEqual(len(videos), 6)

    # Test del parser con un xml sin vídeos
    def test_parser_zero(self):
        xmlFile = open(self.zeroFile, 'r')
        YTParser.parse(xmlFile)
        videos = Item.objects.all()
        self.assertEqual(len(videos), 0)

class TestFlickrParser(TestCase):
    # Inicialización de variables para los test del parser
    def setUp(self):
        self.simpleFile = 'miscosas/testdata/flickr.xml'
        self.zeroFile = 'miscosas/testdata/flickr-0.xml'
        self.oneFile = 'miscosas/testdata/flickr-1.xml'
        self.item_name = "JLGM20200502IMG_4185"
        self.item_link = "https://www.flickr.com/photos/jolugoma/49844651528/"
        self.item_id = "JLGM20200502IMG_4185"
        self.thumbnail = "https://live.staticflickr.com/65535/49844651528_71d56a448b_b.jpg"
        self.alimentador_link = "https://www.flickr.com/photos/tags/fuenlabrada/"
        self.alimentador_name = "fuenlabrada"
        self.like = 0
        self.dislike = 0
        self.puntuation = 0

    # Test del parser con un xml de 1 sólo vídeo. Número de vídeos parseados + chequeo de los elementos parseados
    def test_parser_content(self):
        xmlFile = open(self.oneFile, 'r')
        FlickrParser.parse(xmlFile)
        video = Item.objects.get(item_id = "JLGM20200502IMG_4185") 
        videos = Item.objects.all() 
        self.assertEqual(len(videos), 1)
        self.assertEqual(len(videos), 1)
        self.assertEqual(video.item_link, self.item_link)
        self.assertEqual(video.item_id, self.item_id)
        self.assertEqual(video.thumbnail, self.thumbnail)
        self.assertEqual(video.alimentador_link, self.alimentador_link)
        self.assertEqual(video.alimentador_name, self.alimentador_name)
        self.assertEqual(video.like, self.like)
        self.assertEqual(video.dislike, self.dislike)
        self.assertEqual(video.puntuation, self.puntuation)

    # Test del parser con un xml de 6 vídeos
    def test_parser_count(self):
        xmlFile = open(self.simpleFile, 'r')
        FlickrParser.parse(xmlFile)
        videos = Item.objects.all() 
        self.assertEqual(len(videos), 6)

    # Test del parser con un xml sin vídeos
    def test_parser_zero(self):
        xmlFile = open(self.zeroFile, 'r')
        FlickrParser.parse(xmlFile)
        videos = Item.objects.all()
        self.assertEqual(len(videos), 0)

class TestDGTParser(TestCase):
    # Inicialización de variables para los test del parser
    def setUp(self):
        self.simpleFile = 'miscosas/testdata/dgt.xml'
        self.zeroFile = 'miscosas/testdata/dgt-0.xml'
        self.oneFile = 'miscosas/testdata/dgt-1.xml'
        self.item_name = "La bicicleta, la gran favorita"
        self.item_link = "http://revista.dgt.es/es/noticias/nacional/2020/06JUNIO/0603dia-mundial-de-la-bicicleta.shtml"
        self.item_id = "La bicicleta, la gran favorita"
        self.alimentador_link = "http://revista.dgt.es/es/noticias"
        self.alimentador_name = "Noticias"
        self.description = "<p>Podríamos hablar de una era anterior y otra posterior al COVID-19, sobretodo en lo que se refiere al uso de la bicicleta como modo de transporte. Según una encuesta, la bicicleta podría ser protagonista como vehículo privado en la nueva movilidad.</p>"
        self.like = 0
        self.dislike = 0
        self.puntuation = 0

    # Test del parser con un xml de 1 sólo vídeo. Número de vídeos parseados + chequeo de los elementos parseados
    def test_parser_content(self):
        xmlFile = open(self.oneFile, 'r')
        DGTParser.parse(xmlFile)
        video = Item.objects.get(item_id = "La bicicleta, la gran favorita") 
        videos = Item.objects.all() 
        self.assertEqual(len(videos), 1)
        self.assertEqual(video.item_link, self.item_link)
        self.assertEqual(video.item_id, self.item_id)
        self.assertEqual(video.alimentador_link, self.alimentador_link)
        self.assertEqual(video.alimentador_name, self.alimentador_name)
        #self.assertEqual(video.description, self.description)
        self.assertEqual(video.like, self.like)
        self.assertEqual(video.dislike, self.dislike)
        self.assertEqual(video.puntuation, self.puntuation)


class TestViewsMain(TestCase):

    def setUp(self):
        #Inicialización de variables
        self.item = "t6HMt2fS11s"
        self.alimentadoryt = "UCwY_F_N8pnMvN5I2vqJXwhA"
        self.alimentadorfr = "fuenlabrada"
        self.alimentadordgt = "Noticias"
        self.usuario = "admin"
        
        #Inicialización de BD alimentador YT
        new_alimentador = Alimentador(source = "youtube", alimentador = "UCwY_F_N8pnMvN5I2vqJXwhA", name = "Jéssica Martín Moreno", url = "https://www.youtube.com/channel/UCwY_F_N8pnMvN5I2vqJXwhA", puntuation = 0, count = 0, select = True)
        new_alimentador.save()

        #Inicialización de BD alimentador FR
        new_alimentador = Alimentador(source = "flickr", alimentador = "fuenlabrada", name = "fuenlabrada", url = "https://www.flickr.com/photos/tags/fuenlabrada/", puntuation = 0, count = 0, select = True)
        new_alimentador.save()

        #Inicialización de BD alimentador DGT
        new_alimentador = Alimentador(source = "DGT", alimentador = "Noticias", name = "Noticias", url = "http://revista.dgt.es/es/noticias", puntuation = 0, count = 0, select = True)
        new_alimentador.save()

        #Inicialización de BD Item
        item = Item(alimentador = new_alimentador, item_name = "LA CLAVE DEL ÉXITO, LOS BÁSICOS - SÚPER ENTRENO COVID-19", item_link = "https://www.youtube.com/watch?v=t6HMt2fS11s", item_id = "t6HMt2fS11s", thumbnail = "https://i1.ytimg.com/vi/t6HMt2fS11s/hqdefault.jpg", alimentador_name = "Jéssica Martín Moreno", alimentador_link = "https://www.youtube.com/channelUCwY_F_N8pnMvN5I2vqJXwhA", description = "", like = 0, dislike = 0, puntuation = 0)
        item.save()

        #Incialización BD usuario
        user = "admin"
        password = "admin"
        email = "n.monforte@alumnos.urjc.es"
        user = User.objects.create_user(user, email, password)
        info_usuario = Usuario(usuario=user, foto='/miscosas/perfil0.png',items=0, comentarios=0)
        info_usuario.save()
       

# TEST PARA RECURSOS Y PETICIONES HTTP

    # GET /
    def test_get_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas
    def test_get_main(self):
        response = self.client.get('/miscosas/')
        self.assertEqual(response.status_code, 200)

    # POST /miscosas
    def test_post_main(self):
        response = self.client.post('/miscosas/')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/main.css
    def test_get_css(self):
        response = self.client.get('/miscosas/mains.css')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/rss
    def test_get_rss(self):
        response = self.client.get('/miscosas/rss')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas?format=xml
    def test_get_xml(self):
        response = self.client.get('/miscosas/?format=xml')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas?format=json
    def test_get_json(self):
        response = self.client.get('/miscosas/?format=json')
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/login -> siempre redireccionamos a Inicio -> 302 FOUND
    def test_post_login(self):
        response = self.client.post('/miscosas/login')
        self.assertEqual(response.status_code, 302)

    # GET /miscosas/logout
    def test_get_logout(self):
        response = self.client.get('/miscosas/logout')
        self.assertEqual(response.status_code, 302)

    # GET /miscosas/registration
    def test_get_registration(self):
        response = self.client.get('/miscosas/registration')
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/registration
    def test_post_registration(self):
        response = self.client.post('/miscosas/registration')
        self.assertEqual(response.status_code, 302)

    # GET /miscosas/alimentadores
    def test_get_alimentadores(self):
        response = self.client.get('/miscosas/alimentadores')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/alimentadores?format=xml
    def test_get_alimentadore_xml(self):
        response = self.client.get('/miscosas/alimentadores?format=xml')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/alimentadores?format=json
    def test_get_alimentadores_json(self):
        response = self.client.get('/miscosas/alimentadores?format=json')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/usuarios
    def test_get_usuarios(self):
        response = self.client.get('/miscosas/usuarios')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/usuarios?format=xml
    def test_get_usuarios_xml(self):
        response = self.client.get('/miscosas/usuarios?format=xml')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/usuarios?format=json
    def test_get_usuarios_json(self):
        response = self.client.get('/miscosas/usuarios?format=json')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/informacion
    def test_get_informacion(self):
        response = self.client.get('/miscosas/informacion')
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/<item_id>
    def test_get_item(self):
        response = self.client.get('/miscosas/' + self.item)
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/<item_id>
    def test_post_item(self):
        response = self.client.post('/miscosas/' + self.item)
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/<item_id>
    def test_get_item_error(self):
        response = self.client.get('/miscosas/' + "error")
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/<item_id>
    def test_post_item_error(self):
        response = self.client.post('/miscosas/' + "error")
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/youtube/<alimentador>
    def test_get_ytalimentador(self):
        response = self.client.get('/miscosas/youtube/' + self.alimentadoryt)
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/youtube/<alimentador>
    def test_post_ytalimentador(self):
        response = self.client.post('/miscosas/youtube/' + self.alimentadoryt)
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/youtube/<alimentador>
    def test_get_ytalimentador_error(self):
        response = self.client.get('/miscosas/youtube/' + "error")
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/youtube/<alimentador>
    def test_post_ytalimentador_error(self):
        response = self.client.post('/miscosas/youtube/' + "error")
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/flickr/<alimentador>
    def test_get_fralimentador(self):
        response = self.client.get('/miscosas/flickr/' + self.alimentadorfr)
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/flickr/<alimentador>
    def test_post_fralimentador(self):
        response = self.client.post('/miscosas/flickr/' + self.alimentadorfr)
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/flickr/<alimentador>
    def test_get_fralimentador_error(self):
        response = self.client.get('/miscosas/flickr/' + "error")
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/flickr/<alimentador>
    def test_post_fralimentador_error(self):
        response = self.client.post('/miscosas/flickr/' + "error")
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/DGT/<alimentador>
    def test_get_dgtalimentador(self):
        response = self.client.get('/miscosas/DGT/' + self.alimentadordgt)
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/DGT/<alimentador>
    def test_post_dgtalimentador(self):
        response = self.client.post('/miscosas/DGT/' + self.alimentadordgt)
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/DGT/<alimentador>
    def test_get_dgtalimentador_error(self):
        response = self.client.get('/miscosas/DGT/' + "error")
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/DGT/<alimentador>
    def test_post_dgtalimentador_error(self):
        response = self.client.post('/miscosas/DGT/' + "error")
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/usuarios/<usuario>
    def test_get_usuario(self):
        response = self.client.get('/miscosas/usuarios/' + self.usuario)
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/usuarios/<usuario>
    def test_post_usuario(self):
        response = self.client.post('/miscosas/usuarios/' + self.usuario)
        self.assertEqual(response.status_code, 200)

    # GET /miscosas/usuarios/<usuario>
    def test_get_usuario_error(self):
        response = self.client.get('/miscosas/usuarios/' + "error")
        self.assertEqual(response.status_code, 200)

    # POST /miscosas/usuarios/<usuario>
    def test_post_usuario_error(self):
        response = self.client.post('/miscosas/usuarios/' + "error")
        self.assertEqual(response.status_code, 200)

    # Los recursos /static/miscosas/miscosas.jpg /static/miscosas/favicon.ico  y /media/miscosas/perfil0.png no se pueden comprobar porque en el test no se encuentran los archivos. Se devuelve 404 Not Found

# TEST PARA COMPROBAR CORRESPONDENCIA RECURSO - VISTA (Test para urls.py)
    def test_view_index(self):
        response = self.client.get('/')
        self.assertEqual(response.resolver_match.func, views.index)

    def test_view_main(self):
        response = self.client.get('/miscosas/')
        self.assertEqual(response.resolver_match.func, views.main)

    def test_view_main_xml(self):
        response = self.client.get('/miscosas/?format=xml')
        self.assertEqual(response.resolver_match.func, views.main)

    def test_view_main_json(self):
        response = self.client.get('/miscosas/?format=json')
        self.assertEqual(response.resolver_match.func, views.main)

    def test_view_login(self):
        response = self.client.get('/miscosas/login')
        self.assertEqual(response.resolver_match.func, views.login_view)

    def test_view_css(self):
        response = self.client.get('/miscosas/main.css')
        self.assertEqual(response.resolver_match.func, views.css_view)

    def test_view_logout(self):
        response = self.client.get('/miscosas/logout')
        self.assertEqual(response.resolver_match.func, views.logout_view)

    def test_view_registration(self):
        response = self.client.get('/miscosas/registration')
        self.assertEqual(response.resolver_match.func, views.registration)

    def test_view_alimentadores(self):
        response = self.client.get('/miscosas/alimentadores')
        self.assertEqual(response.resolver_match.func, views.alimentadores)

    def test_view_alimentadores_xml(self):
        response = self.client.get('/miscosas/alimentadores?format=xml')
        self.assertEqual(response.resolver_match.func, views.alimentadores)

    def test_view_alimentadores_json(self):
        response = self.client.get('/miscosas/alimentadores?format=json')
        self.assertEqual(response.resolver_match.func, views.alimentadores)

    def test_view_usuarios(self):
        response = self.client.get('/miscosas/usuarios')
        self.assertEqual(response.resolver_match.func, views.usuarios)

    def test_view_usuarios_xml(self):
        response = self.client.get('/miscosas/usuarios?format=xml')
        self.assertEqual(response.resolver_match.func, views.usuarios)

    def test_view_usuarios_json(self):
        response = self.client.get('/miscosas/usuarios?format=json')
        self.assertEqual(response.resolver_match.func, views.usuarios)

    def test_view_usuario(self):
        response = self.client.get('/miscosas/usuarios/' + self.usuario)
        self.assertEqual(response.resolver_match.func, views.usuario)

    def test_view_informacion(self):
        response = self.client.get('/miscosas/informacion')
        self.assertEqual(response.resolver_match.func, views.informacion)

    def test_view_alimentadorYT(self):
        response = self.client.get('/miscosas/youtube/' + self.alimentadoryt)
        self.assertEqual(response.resolver_match.func, views.yt_alimentador)

    def test_view_alimentadorFR(self):
        response = self.client.get('/miscosas/flickr/' + self.alimentadorfr)
        self.assertEqual(response.resolver_match.func, views.flickr_alimentador)

    def test_view_alimentadorDGT(self):
        response = self.client.get('/miscosas/DGT/' + self.alimentadordgt)
        self.assertEqual(response.resolver_match.func, views.DGT_alimentador)

    def test_view_item(self):
        response = self.client.get('/miscosas/' + self.item)
        self.assertEqual(response.resolver_match.func, views.item_view)

# TEST PARA COMPROBAR LOS TEMPLATES QUE SIRVE CADA VISTA
    def test_html_index(self):
        checks = ["<h1>Mis aplicaciones</h1>"]
        response = self.client.get('/')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_main(self):
        checks = ["<h1>Página de Inicio de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_registration(self):
        checks = ["<h1>Página de Registro de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/registration')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_alimentadores(self):
        checks = ["<h1>Página de Alimentadores de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/alimentadores')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_usuarios(self):
        checks = ["<h1>Página de Usuarios de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/usuarios')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_usuario(self):
        checks = ["<h1>Página de Usuario de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/usuarios/' + self.usuario)
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_usuario_error(self):
        checks = ["<h1>Página de Error de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/usuarios/error')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_item(self):
        checks = ["<h1>Página de Item de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/' + self.item)
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_item_error(self):
        checks = ["<h1>Página de Error de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/error')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_alimentador_YT(self):
        checks = ["<h1>Página de Alimentador de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/youtube/' + self.alimentadoryt)
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_alimentador_Flickr(self):
        checks = ["<h1>Página de Alimentador de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/flickr/' + self.alimentadorfr)
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_alimentador_DGT(self):
        checks = ["<h1>Página de Alimentador de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/DGT/' + self.alimentadordgt)
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_alimentador_YT_error(self):
        checks = ["<h1>Página de Error de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/youtube/error')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_alimentador_Flickr_error(self):
        checks = ["<h1>Página de Error de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/flickr/error')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    def test_html_alimentador_DGT_error(self):
        checks = ["<h1>Página de Error de MIS COSAS</h1>"]
        response = self.client.get('/miscosas/DGT/error')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)





