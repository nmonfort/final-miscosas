from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.template import loader
from django.contrib.auth import logout, login, authenticate
from django.db import IntegrityError
from django.contrib.auth.models import User
from .models import Alimentador
from .models import Item
from .models import Usuario
from .models import Voto_Usuario
from .models import Comentario
from .models import CSS
from .models import TamanoLetra
from .models import Estilo
from .parseryt import YTParser
from .parserflickr import FlickrParser
from .parserdgt import DGTParser
from .forms import AlimentadorForm
from .forms import LoginForm
from .forms import RegistrationForm
from .forms import ComentarioForm
from .forms import CSSForm
from .forms import FotoForm
import urllib.request

# Create your views here.
@csrf_exempt
def get_language(request):
    language = request.META.get('HTTP_ACCEPT_LANGUAGE', None)
    if language is not None:
        language = language.split(',')[0]
    else:
        #Si no hay lenguaje, por defecto se utiliza el español
        language = "es-ES"
    return language

@csrf_exempt
def index(request):
    language = get_language(request)

    return render(request, "miscosas/" + language + "/index.html")

@csrf_exempt
def login_view(request):
    language = get_language(request)

    form = LoginForm(request.POST)
    if form.is_valid():
        user = form.cleaned_data['User']
        password = form.cleaned_data['Password']
        user = authenticate(request, username=user, password=password)
        if user is not None:
            login(request, user)
        else:
            loginform = LoginForm()
            if language == "es-ES":
                error = "Fallo de autenticación. No existe el usuario o la contraseña no es correcta."
            elif language == "en-GB":
                error = "Authentication failure. The user does not exist or password is incorrect."
            context = {'error': error, 'loginform': loginform}
            return render(request, "miscosas/" + language + "/error.html", context)

    return redirect("/miscosas/")

@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect("/miscosas/")

@csrf_exempt
def registration(request):
    language = get_language(request)

    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.cleaned_data['User']
            password = form.cleaned_data['Password']
            email = form.cleaned_data['Email']
            try:
                user = User.objects.create_user(user, email, password)
                info_usuario = Usuario(usuario=user, foto='/miscosas/perfil0.png',items=0, comentarios=0)
                info_usuario.save()
                user = authenticate(request, username=user, password=password)
                if user is not None:
                    login(request, user)
                return redirect("/miscosas/")

            except IntegrityError:
                if language == "es-ES":
                    error = "El nombre de usuario ya existe."
                elif language == "en-GB":
                    error = "The username already exists."
                context = {'form': form, 'error': error}
                return render(request, "miscosas/" + language + "/registration.html", context)
        else:
            return redirect("/miscosas/registration")

    elif request.method == "GET":
        form = RegistrationForm()
        context = {'form': form, 'error': ""}
        return render(request, "miscosas/" + language + "/registration.html", context)

@csrf_exempt
def votacion(request):
    # POST para guardar un like
    if 'Like' in request.POST:
        item_name = request.POST['Like']
        item = Item.objects.get(item_name = item_name)
        alimentador = Alimentador.objects.get(alimentador = item.alimentador)
        # Se comprueba si el user ya puntuó ese item
        try:
            usuario = User.objects.get(username=request.user)
            voto = Voto_Usuario.objects.get(item = item, usuario = usuario)
            # Si el user ya votó negativamente el item
            if voto.like == False:
                voto.like = True

                # Actualización de la puntuación general del item
                item.like = item.like + 1
                item.dislike = item.dislike - 1
                item.puntuation = item.puntuation + 2
                item.save()

                voto.puntuation = item.puntuation
                voto.time = timezone.now()
                voto.save()

                alimentador.puntuation = alimentador.puntuation + 2
                alimentador.save()

        # Si el user no ha votado este item anteriormente
        except:
            voto = Voto_Usuario(item = item, usuario = usuario, like = True, time = timezone.now())

            # Actualización de la puntuación general del item
            item.like = item.like + 1
            item.puntuation = item.puntuation + 1
            item.save()

            voto.puntuation = item.puntuation
            voto.save()

            alimentador.puntuation = alimentador.puntuation + 1
            alimentador.save()

            # Actualización del número de items votados por el user
            usuario = User.objects.get(username=request.user)
            usuario = Usuario.objects.get(usuario = usuario)
            usuario.items = usuario.items + 1
            usuario.save()


    # POST para guardar un unlike
    if 'Dislike' in request.POST:
        item_name = request.POST['Dislike']
        item = Item.objects.get(item_name = item_name)
        alimentador = Alimentador.objects.get(alimentador = item.alimentador)

        # Se comprueba si el user ya puntuó ese item
        try:
            usuario = User.objects.get(username=request.user)
            voto = Voto_Usuario.objects.get(item = item, usuario = usuario)

            # Si el user ya votó positivamene el item
            if voto.like == True:
                voto.like = False

                # Actualización de la puntuación general del item
                item.like = item.like - 1
                item.dislike = item.dislike + 1
                item.puntuation = item.puntuation - 2
                item.save()

                voto.puntuation = item.puntuation
                voto.time = timezone.now()
                voto.save()

                alimentador.puntuation = alimentador.puntuation - 2
                alimentador.save()

        # Si el user no ha votado este item anteriormente
        except:
            voto = Voto_Usuario(item = item, usuario = usuario, like = False, time = timezone.now())

            # Actualización de la puntuación general del item
            item.dislike = item.dislike + 1
            item.puntuation = item.puntuation - 1
            item.save()

            voto.puntuation = item.puntuation
            voto.save()

            alimentador.puntuation = alimentador.puntuation - 1
            alimentador.save()

            # Actualización del número de items votados por el user
            usuario = User.objects.get(username=request.user)
            usuario = Usuario.objects.get(usuario = usuario)
            usuario.items = usuario.items + 1
            usuario.save()

valido = False
@csrf_exempt
def main(request):
    language = get_language(request)

    global valido
    if request.method == "POST":
        # POST de un alimentador de YOUTUBE
        if 'alimentadorform' in request.POST:
            form = AlimentadorForm(request.POST)
            if form.is_valid():
                source = form.cleaned_data['Source']
                try:
                    if str(source) == "flickr":
                        alimentador = form.cleaned_data['Channel']
                        if alimentador != "":
                            info_alimentador = Alimentador.objects.get(alimentador = alimentador.lower())
                    elif str(source) == "youtube":
                        alimentador = form.cleaned_data['Channel']
                        if alimentador != "":
                            info_alimentador = Alimentador.objects.get(alimentador = alimentador)
                    elif str(source) == "DGT":
                        alimentador = form.cleaned_data['Channel_2']
                        if alimentador != None:
                            info_alimentador = Alimentador.objects.get(alimentador = alimentador)

                    if alimentador != "" and alimentador != None:
                        info_alimentador.select = True
                        info_alimentador.save()
                    else:
                        loginform = LoginForm()
                        if language == "es-ES":
                            error = "Introduce el alimentador apropiado para la fuente seleccionada."
                        elif language == "en-GB":
                            error = "Insert the appropriate feeder for the selected source."
                        context = {'error': error, 'loginform': loginform}
                        return render(request, "miscosas/" + language + "/error.html", context)
                        
                except:
                    pass

                # Redirección a la página del alimentador pedido -> VISTA ALIMENTADOR
                valido = True
                return redirect("/miscosas/" + str(source) + "/" + str(alimentador))

        # POST para eliminar un alimentador de INICIO
        if 'Eliminar' in request.POST:
            alimentador_name = request.POST['Eliminar']
            alimentador = Alimentador.objects.get(name = alimentador_name)
            alimentador.select = False
            alimentador.save()

        # POST para seleccionar un alimentador de INICIO
        if 'Seleccionar' in request.POST:
            alimentador_name = request.POST['Seleccionar']
            alimentador = Alimentador.objects.get(name = alimentador_name)
            alimentador.select = True
            alimentador.save()

            valido = True
            return redirect("/miscosas/" + alimentador.source + "/"+ alimentador.alimentador)

        # POST para guardar un like
        if 'Like' in request.POST or 'Dislike' in request.POST:
            votacion(request)

    # Creación de formularios de inicio
    form = AlimentadorForm()
    loginform = LoginForm()

    # Inicialización contexto plantilla
    alimentador_list = Alimentador.objects.filter(select=True)
    items = Item.objects.all()
    items_mas_puntuados = items.order_by('puntuation').exclude(like=0, dislike=0)
    items_mas_puntuados = items_mas_puntuados.reverse()[:10]
    context = {'form': form, 'loginform': loginform, 'alimentador_list': alimentador_list, 'items_mas_puntuados': items_mas_puntuados}

    if request.user.is_authenticated:
        usuario = User.objects.get(username=request.user)
        items_usuario = Voto_Usuario.objects.filter(usuario = usuario)
        items_usuario = items_usuario.order_by('time')
        items_usuario = items_usuario.reverse()[:5]

        context = {'form': form, 'loginform': loginform, 'alimentador_list': alimentador_list, 'items_mas_puntuados': items_mas_puntuados, 'items_usuario': items_usuario}
        
    if "xml" in request.META['QUERY_STRING']:
        return render(request, "miscosas/" + language + '/base.xml', context, content_type='text/xml')
    elif "json" in request.META['QUERY_STRING']:
        return render(request, "miscosas/" + language + '/base.json', context, content_type='text/json')

    return render(request, "miscosas/" + language + "/base.html", context)
    
@csrf_exempt
def yt_alimentador(request, alimentador):
    global valido    
    language = get_language(request)

    # Comprobar si es un alimentador ya seleccionado
    try: 
        info_alimentador = Alimentador.objects.get(alimentador = alimentador)
    except:
        if not valido:
            loginform = LoginForm()
            if language == "es-ES":
                error = "Este alimentador no ha sido seleccionado."
            elif language == "en-GB":
                error = "This feeder has not been selected."
            context = {'error': error, 'loginform': loginform}
            return render(request, "miscosas/" + language + "/error.html", context)

    # Parseo el XML
    url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + alimentador
    try:
        xmlStream = urllib.request.urlopen(url)
    except:
        valido = False
        loginform = LoginForm()
        if language == "es-ES":
            error = "No existe el alimentador."
        elif language == "en-GB":
            error = "Feeder doesn't exist."
        context = {'error': error, 'loginform': loginform}
        return render(request, "miscosas/" + language + "/error.html", context)
    YTParser.parse(xmlStream)

    # Datos del alimentador
    info_alimentador = Alimentador.objects.get(alimentador = alimentador)

    # Lista de items pertenecientes al alimentador
    item_list = Item.objects.filter(alimentador = info_alimentador)

    # Actualización del número de items del alimentador
    info_alimentador.count = item_list.count()
    info_alimentador.save()

    if request.method == 'POST':
        # POST para eliminar un alimentador de ALIMENTADORES
        if 'Eliminar' in request.POST:
            alimentador_name = request.POST['Eliminar']
            info_alimentador = Alimentador.objects.get(name = alimentador_name)
            info_alimentador.select = False
            info_alimentador.save()

        # POST para seleccionar un alimentador de ALIMENTADORES
        if 'Seleccionar' in request.POST:
            alimentador_name = request.POST['Seleccionar']
            info_alimentador = Alimentador.objects.get(name = alimentador_name)
            info_alimentador.select = True
            info_alimentador.save()

            valido = True
            return redirect("/miscosas/" + info_alimentador.source + "/"+ info_alimentador.alimentador)

        # POST para guardar un like
        if 'Like' in request.POST or 'Dislike' in request.POST:
            votacion(request)

    # Contexto de la plantilla: lista de items + alimentador utilizado
    loginform = LoginForm()
    context = {'item_list': item_list, 'alimentador': info_alimentador, 'loginform': loginform}
    valido = False
    return render(request, "miscosas/" + language + "/alimentador.html", context)

@csrf_exempt
def flickr_alimentador(request, alimentador):
    global valido
    language = get_language(request)

    try: 
        info_alimentador = Alimentador.objects.get(alimentador = alimentador)
    except:
        if not valido:
            loginform = LoginForm()
            if language == "es-ES":
                error = "Este alimentador no ha sido seleccionado."
            elif language == "en-GB":
                error = "This feeder has not been selected."
            context = {'error': error, 'loginform': loginform}
            return render(request, "miscosas/" + language + "/error.html", context)

    # Parseo el XML
    url = 'https://www.flickr.com/services/feeds/photos_public.gne?tags=' + alimentador
    try:
        xmlStream = urllib.request.urlopen(url)
    except:
        loginform = LoginForm()
        if language == "es-ES":
            error = "No existe el alimentador."
        elif language == "en-GB":
            error = "Feeder doesn't exist."
        context = {'error': error, 'loginform': loginform}
        return render(request, "miscosas/" + language + "/error.html", context)
    FlickrParser.parse(xmlStream)

    # Datos del alimentador
    info_alimentador = Alimentador.objects.get(alimentador = alimentador.lower())

    # Lista de items pertenecientes al alimentador
    item_list = Item.objects.filter(alimentador = info_alimentador)

    # Actualización del número de items del alimentador
    info_alimentador.count = item_list.count()
    info_alimentador.save()

    if request.method == 'POST':
        # POST para eliminar un alimentador de ALIMENTADORES
        if 'Eliminar' in request.POST:
            alimentador_name = request.POST['Eliminar']
            info_alimentador = Alimentador.objects.get(name = alimentador_name)
            info_alimentador.select = False
            info_alimentador.save()

        # POST para seleccionar un alimentador de ALIMENTADORES
        if 'Seleccionar' in request.POST:
            alimentador_name = request.POST['Seleccionar']
            info_alimentador = Alimentador.objects.get(name = alimentador_name)
            info_alimentador.select = True
            info_alimentador.save()

            valido = True
            return redirect("/miscosas/" + info_alimentador.source + "/"+ info_alimentador.alimentador)

        # POST para guardar un like
        if 'Like' in request.POST or 'Dislike' in request.POST:
            votacion(request)

    # Contexto de la plantilla: lista de items + alimentador utilizado
    loginform = LoginForm()
    context = {'item_list': item_list, 'alimentador': info_alimentador, 'loginform': loginform}
    valido = False
    return render(request, "miscosas/" + language + "/alimentador.html", context)

@csrf_exempt
def DGT_alimentador(request, alimentador):
    global valido
    language = get_language(request)

    try: 
        info_alimentador = Alimentador.objects.get(alimentador = alimentador)
    except:
        if not valido:
            loginform = LoginForm()
            if language == "es-ES":
                error = "Este alimentador no ha sido seleccionado."
            elif language == "en-GB":
                error = "This feeder has not been selected."
            context = {'error': error, 'loginform': loginform}
            return render(request, "miscosas/" + language + "/error.html", context)

    # Parseo el XML
    if alimentador == "Opinión":
        # Para evitar problema con "´"
        url = 'http://revista.dgt.es/es/rss/opinion.xml'
    else:
        url = 'http://revista.dgt.es/es/rss/' + alimentador.lower() + '.xml'

    try:
        xmlStream = urllib.request.urlopen(url)
    except:
        loginform = LoginForm()
        if language == "es-ES":
            error = "No existe el alimentador."
        elif language == "en-GB":
            error = "Feeder doesn't exist."
        context = {'error': error, 'loginform': loginform}
        return render(request, "miscosas/" + language + "/error.html", context)
    DGTParser.parse(xmlStream)

    # Datos del alimentador
    info_alimentador = Alimentador.objects.get(alimentador = alimentador)

    # Lista de items pertenecientes al alimentador
    item_list = Item.objects.filter(alimentador = info_alimentador)

    # Actualización del número de items del alimentador
    info_alimentador.count = item_list.count()
    info_alimentador.save()

    if request.method == 'POST':
        # POST para eliminar un alimentador de ALIMENTADORES
        if 'Eliminar' in request.POST:
            alimentador_name = request.POST['Eliminar']
            info_alimentador = Alimentador.objects.get(name = alimentador_name)
            info_alimentador.select = False
            info_alimentador.save()

        # POST para seleccionar un alimentador de ALIMENTADORES
        if 'Seleccionar' in request.POST:
            alimentador_name = request.POST['Seleccionar']
            info_alimentador = Alimentador.objects.get(name = alimentador_name)
            info_alimentador.select = True
            info_alimentador.save()

            valido = True
            return redirect("/miscosas/" + info_alimentador.source + "/"+ info_alimentador.alimentador)

        # POST para guardar un like
        if 'Like' in request.POST or 'Dislike' in request.POST:
            votacion(request)

    # Contexto de la plantilla: lista de items + alimentador utilizado
    loginform = LoginForm()
    context = {'item_list': item_list, 'alimentador': info_alimentador, 'loginform': loginform}
    valido = False
    return render(request, "miscosas/" + language + "/alimentador.html", context)

@csrf_exempt
def alimentadores(request):
    language = request.META.get('HTTP_ACCEPT_LANGUAGE', None)
    language = get_language(request)

    alimentador_list = Alimentador.objects.all()

    if "xml" in request.META['QUERY_STRING']:
        context = {'alimentador_list': alimentador_list}
        return render(request, "miscosas/" + language + '/alimentadores.xml', context, content_type='text/xml')
    elif "json" in request.META['QUERY_STRING']:
        context = {'alimentador_list': alimentador_list}
        return render(request, "miscosas/" + language + '/alimentadores.json', context, content_type='text/json')

    loginform = LoginForm()
    context = {'alimentador_list': alimentador_list, 'pagina': "Alimentadores", 'loginform': loginform}
    return render(request, "miscosas/" + language + "/alimentadores.html", context)


@csrf_exempt
def usuarios(request):
    language = get_language(request)

    user_list = Usuario.objects.all()

    if "xml" in request.META['QUERY_STRING']:
        context = {'user_list': user_list}
        return render(request, "miscosas/" + language + '/usuarios.xml', context, content_type='text/xml')
    elif "json" in request.META['QUERY_STRING']:
        context = {'user_list': user_list}
        return render(request, "miscosas/" + language + '/usuarios.json', context, content_type='text/json')

    loginform = LoginForm()
    context = {'user_list': user_list, 'pagina': "Usuarios", 'loginform': loginform}
    return render(request, "miscosas/" + language + "/usuarios.html", context)

@csrf_exempt
def usuario(request, user):
    language = get_language(request)

    if request.method == "POST":
        if 'fotoform' in request.POST:
            form = FotoForm(request.POST, request.FILES)
            if form.is_valid():
                foto = form.cleaned_data['foto']
                usuario = User.objects.get(username=request.user)
                usuario = Usuario.objects.get(usuario = usuario)
                usuario.foto = foto
                usuario.save()

        if 'CSSform' in request.POST:

            form = CSSForm(request.POST, request.FILES)
            if form.is_valid():
                estilo = form.cleaned_data['estilo']
                tamaño = form.cleaned_data['tamaño']

                # Si no se ha rellenado alguno de los campos, se deja el valor por defecto: ligero y mediano
 
                try:
                    usuario = User.objects.get(username=request.user)
                    css = CSS.objects.get(usuario = usuario)
                    if estilo == None:
                        css.tamaño = tamaño
                    elif tamaño == None:
                        css.estilo = estilo
                    else:
                        css.estilo = estilo
                        css.tamaño = tamaño
                except:
                    if estilo == None:
                        css = CSS(usuario = usuario, estilo = Estilo.objects.get(estilo="Ligero"), tamaño = tamaño)
                    elif tamaño == None:
                        css = CSS(usuario = usuario, estilo = estilo, tamaño = TamanoLetra.objects.get(tamano="Mediano"))
                    else:
                        css = CSS(usuario = usuario, estilo = estilo, tamaño = tamaño)
                css.save()

    loginform = LoginForm()
    try:
        usuario = User.objects.get(username=user)
    except:
        if language == "es-ES":
            error = "El usuario no está registrado."
        elif language == "en-GB":
            error = "User is not registered."
        context = {'error': error, 'loginform': loginform}
        return render(request, "miscosas/" + language + "/error.html", context)
    form = CSSForm()
    fotoform = FotoForm()
    info_usuario = Usuario.objects.get(usuario=usuario)
    list_like = Voto_Usuario.objects.filter(usuario = usuario)
    list_comentario = Comentario.objects.filter(usuario = usuario)
    if request.user.is_authenticated:
        path = "/miscosas/usuarios/" + str(request.user)
    else:
        path = ""
    context = {'form': form, 'fotoform': fotoform, 'list_like': list_like, 'list_comentario': list_comentario, 'info_usuario': info_usuario, 'path': path, 'loginform': loginform}

    return render(request, "miscosas/" + language + "/usuario.html", context)

@csrf_exempt
def informacion(request):
    language = get_language(request)

    loginform = LoginForm()
    context = {'loginform': loginform}
    return render(request, "miscosas/" + language + "/informacion.html", context)

@csrf_exempt
def item_view(request, item):
    language = get_language(request)

    if request.method == 'POST':
        if 'comentarioform' in request.POST:
            form = ComentarioForm(request.POST, request.FILES)
            if form.is_valid():
                comentario = form.cleaned_data['Comentario']
                foto = form.cleaned_data['foto']
                info_item = Item.objects.get(item_id = item)
                usuario = User.objects.get(username=request.user)
                c = Comentario(item = info_item, usuario = usuario, comentario = comentario, foto = foto, fecha = timezone.now())
                c.save()

            # Actualización del número de comentarios de un usuario
            usuario = Usuario.objects.get(usuario = usuario)
            usuario.comentarios = usuario.comentarios + 1
            usuario.save()

        # POST para guardar un like
        if 'Like' in request.POST or 'Dislike' in request.POST:
            votacion(request)

    try:
        info_item = Item.objects.get(item_id = item)
    except:
        loginform = LoginForm()
        if language == "es-ES":
            error = "El item no existe."
        elif language == "en-GB":
            error = "The item does not exist."
        context = {'error': error, 'loginform': loginform}
        return render(request, "miscosas/" + language + "/error.html", context)

    info_alimentador = Alimentador.objects.get(name = info_item.alimentador_name)
    form = ComentarioForm()
    loginform = LoginForm()
    item = Item.objects.get(item_id = item)
    comentario_list = Comentario.objects.filter(item = item)
    context = {'info_item': info_item, 'info_alimentador': info_alimentador,'form': form, 'comentario_list': comentario_list, 'loginform': loginform}

    return render(request, "miscosas/" + language + "/item.html", context)

@csrf_exempt
def css_view(request):
    language = get_language(request)

    if request.user.is_authenticated:
        try:
            usuario = User.objects.get(username=request.user)
            css = CSS.objects.get(usuario = usuario)

            if str(css.estilo) == "Ligero":
                estilo1 = '#c7f1f9'
                estilo2 = '#e9ecef'
                estilo3 = '#64cfe3'
                estilo4 = 'white'
                color = "black"
                luminosidad = 1
            elif str(css.estilo) == "Oscuro":
                estilo1 = '#1b245f'
                estilo2 = '#373944'
                estilo3 = 'black'
                estilo4 = 'black'
                color = "white"
                luminosidad = 0.2

            if str(css.tamaño) == "Pequeño":
                tamaño1 = "10"
                tamaño2 = "14"
                tamaño3 = "18"
                tamaño4 = "22"
                padding = "6"
                height = "60"
            elif str(css.tamaño) == "Mediano":
                tamaño1 = "16"
                tamaño2 = "20"
                tamaño3 = "24"
                tamaño4 = "28"
                padding = "8"
                height = "60"
            elif str(css.tamaño) == "Grande":
                tamaño1 = "30"
                tamaño2 = "34"
                tamaño3 = "38"
                tamaño4 = "42"
                padding = "10"
                height = "150"

        except:
         # Si el usuario no ha definido estilo, se establecen valores por defecto
            estilo1 = '#c7f1f9'
            estilo2 = '#e9ecef'
            estilo3 = '#64cfe3'
            estilo4 = 'white'
            color = "black"
            luminosidad = 1
            tamaño1 = "16"
            tamaño2 = "20"
            tamaño3 = "24"
            tamaño4 = "28"
            padding = "8"
            height = "60"

    else:
    # Si el usuario no esta autenticado se establecen valores por defecto
        estilo1 = '#c7f1f9'
        estilo2 = '#e9ecef'
        estilo3 = '#64cfe3'
        estilo4 = 'white'
        color = "black"
        luminosidad = 1
        tamaño1 = "16"
        tamaño2 = "20"
        tamaño3 = "24"
        tamaño4 = "28"
        padding = "8"
        height = "60"

    context = {'estilo1': estilo1,'estilo2': estilo2, 'estilo3': estilo3, 'estilo4': estilo4,'tamaño1': tamaño1, 'tamaño2': tamaño2, 'tamaño3': tamaño3, 'tamaño4': tamaño4,'color': color, 'padding': padding, 'height': height, 'luminosidad': luminosidad}
    return render(request, "miscosas/" + language + "/main.css", context, content_type='text/css')

@csrf_exempt
def rss(request):
    language = get_language(request)

    lista_comentarios = Comentario.objects.all()
    context = {'lista_comentarios': lista_comentarios}
    return (render(request, "miscosas/" + language + '/comentarios.rss', context, content_type="application/rss+xml"))
