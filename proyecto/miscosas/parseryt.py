from xml.sax.handler import ContentHandler
from xml.sax import make_parser

from .models import Alimentador
from .models import Item

# Create your views here.

class YTHandler(ContentHandler):
    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.name = ""
        self.link = ""
        self.thumbnail = ""
        self.uri = ""
        self.channel = ""
        self.description = ""
        self.published = ""
        self.videoId = ""
        self.channelId = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
            elif name == 'media:thumbnail':
                self.thumbnail = attrs.get('url')
            elif name == 'uri':
                self.inContent = True
            elif name == 'name':
                self.inContent = True
            elif name == 'media:description':
                self.inContent = True
            elif name == 'published':
                self.inContent = True
            elif name == 'yt:videoId':
                self.inContent = True
            elif name == 'yt:channelId':
                self.inContent = True

    def endElement (self, name):

        if name == 'entry':
            self.inEntry = False

            # ACTUALIZACIÓN DE LAS BBDD
            try:
                new_alimentador = Alimentador.objects.get(alimentador = self.channelId) 
            except:
                new_alimentador = Alimentador(source = "youtube", alimentador = self.channelId, name = self.channel, url = self.uri, puntuation = 0, count = 0, select = True)
                new_alimentador.save()

            try:
               item = Item.objects.get(item_link = self.link) 
            except:
                item = Item(alimentador = new_alimentador, item_name = self.name, item_link = self.link, item_id = self.videoId, thumbnail = self.thumbnail, alimentador_name = self.channel, alimentador_link = self.uri, description = self.description, like = 0, dislike = 0, puntuation = 0)
                item.save()

                
        elif self.inEntry:
            if name == 'title':
                self.name = self.content
            elif name == 'uri':
                self.uri = self.content
            elif name == 'name':
                self.channel = self.content
            elif name == 'media:description':
                self.description = self.content
            elif name == 'published':
                self.published = self.content
            elif name == 'yt:channelId':
                self.channelId = self.content
            elif name == 'yt:videoId':
                self.videoId = self.content

            self.content = ""
            self.inContent = False

    def characters (self, chars):
       if self.inContent:
            self.content = self.content + chars

# Load parser and driver
YTParser = make_parser()
YTParser.setContentHandler(YTHandler())

