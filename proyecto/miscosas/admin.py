from django.contrib import admin

# Register your models here.

from .models import Alimentador
from .models import FuenteAlimentador
from .models import Usuario
from .models import Item
from .models import Voto_Usuario
from .models import Comentario
from .models import TamanoLetra
from .models import Estilo
from .models import CSS
from .models import DGT

admin.site.register(Alimentador)
admin.site.register(FuenteAlimentador)
admin.site.register(Usuario)
admin.site.register(Item)
admin.site.register(Voto_Usuario)
admin.site.register(Comentario)
admin.site.register(CSS)
admin.site.register(TamanoLetra)
admin.site.register(Estilo)
admin.site.register(DGT)
