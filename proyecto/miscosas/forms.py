from django import forms

from .models import FuenteAlimentador
from .models import DGT
from .models import TamanoLetra
from .models import Usuario
from .models import Estilo

class AlimentadorForm(forms.Form):
    Source =  forms.ModelChoiceField(label="Source", queryset=FuenteAlimentador.objects.all())
    Channel = forms.CharField(label="Youtube or Flickr feeder", required=False)
    Channel_2 = forms.ModelChoiceField(label="DGT feeder", queryset=DGT.objects.all(), required=False) 

class LoginForm(forms.Form): 
    User = forms.CharField()
    Password = forms.CharField(widget=forms.PasswordInput)

class RegistrationForm(forms.Form): 
    User = forms.CharField()
    Password = forms.CharField(widget=forms.PasswordInput)
    Email = forms.CharField()

class ComentarioForm(forms.Form): 
    Comentario = forms.CharField()
    foto = forms.ImageField(label="", required=False)

class CSSForm(forms.Form): 
    estilo = forms.ModelChoiceField(queryset = Estilo.objects.all(), required=False)
    tamaño = forms.ModelChoiceField(queryset = TamanoLetra.objects.all(), required=False)

class FotoForm(forms.Form):
    foto = forms.ImageField(label="")
