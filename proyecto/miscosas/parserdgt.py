from xml.sax.handler import ContentHandler
from xml.sax import make_parser

from .models import Alimentador
from .models import Item

# Create your views here.

class DGTHandler(ContentHandler):
    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.item_name = ""
        self.item_link = ""
        self.item_thumbnail = ""
        self.alimentador_name = ""
        self.alimentador_link = "http://revista.dgt.es/es/"
        self.item_description = ""
        self.item_published = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif name == 'title':
            self.inContent = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.item_link = attrs.get('href')
            elif name == 'summary':
                self.inContent = True
            elif name == 'published':
                self.inContent = True

    def endElement (self, name):

        if name == 'entry':
            self.inEntry = False

            # ACTUALIZACIÓN DE LAS BBDD
            
            try:
                new_alimentador = Alimentador.objects.get(alimentador = self.alimentador_name) 
            except:
                if self.alimentador_name == "Opinión":
                    new_alimentador = Alimentador(source = "DGT", alimentador = self.alimentador_name, name = self.alimentador_name, url = self.alimentador_link + 'opinion', puntuation = 0, count = 0, select = True)
		        
                else:
                    new_alimentador = Alimentador(source = "DGT", alimentador = self.alimentador_name, name = self.alimentador_name, url = self.alimentador_link + self.alimentador_name.lower(), puntuation = 0, count = 0, select = True)

                new_alimentador.save()   

            try:
               item = Item.objects.get(item_link = self.item_link) 
            except:
                if self.alimentador_name == "Opinión":
                    item = Item(alimentador = new_alimentador, item_name = self.item_name, item_link = self.item_link, item_id = self.item_name, thumbnail = self.item_thumbnail, alimentador_name = self.alimentador_name, alimentador_link = self.alimentador_link + 'opinion', description = self.item_description, like = 0, dislike = 0, puntuation = 0)
                else:
                    item = Item(alimentador = new_alimentador, item_name = self.item_name, item_link = self.item_link, item_id = self.item_name, thumbnail = self.item_thumbnail, alimentador_name = self.alimentador_name, alimentador_link = self.alimentador_link + self.alimentador_name.lower(), description = self.item_description, like = 0, dislike = 0, puntuation = 0)
                item.save()
       
        elif self.inEntry:
            if name == 'title':
                self.item_name = self.content
            elif name == 'summary':
                self.item_description = self.content
            elif name == 'published':
                self.item_published = self.content
 
            self.content = ""
            self.inContent = False

        elif name == 'title':
            self.alimentador_name = self.content

            self.content = ""
            self.inContent = False

    def characters (self, chars):
       if self.inContent:
            self.content = self.content + chars

# Load parser and driver
DGTParser = make_parser()
DGTParser.setContentHandler(DGTHandler())

