from xml.sax.handler import ContentHandler
from xml.sax import make_parser

from .models import Alimentador
from .models import Item

# Create your views here.

class FlickrHandler(ContentHandler):
    def __init__ (self):
        videos = []
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.photo_name = ""
        self.photo_link = ""
        self.thumbnail = ""
        self.tag_name = ""
        self.tag_link = ""

    def startElement (self, name, attrs):
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                if attrs.get('rel') == "enclosure":
                    self.thumbnail = attrs.get('href')
                elif attrs.get('rel') == "alternate":
                    self.photo_link = attrs.get('href')
        else:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                if attrs.get('rel') == "alternate":
                    self.tag_link = attrs.get('href')
            
    def endElement (self, name):
        if name == 'entry':
            self.inEntry = False

            # ACTUALIZACIÓN DE LAS BBDD
            try:
                new_alimentador = Alimentador.objects.get(alimentador = self.tag_name) 
            except:
                new_alimentador = Alimentador(source = "flickr", alimentador = self.tag_name, name = self.tag_name, url = self.tag_link, puntuation = 0, count = 0, select=True)
                new_alimentador.save()
            try:
               item = Item.objects.get(item_link = self.photo_link) 
            except:
                item = Item(alimentador = new_alimentador, item_name = self.photo_name, item_link = self.photo_link, item_id = self.photo_name, thumbnail = self.thumbnail, alimentador_name = self.tag_name, alimentador_link = self.tag_link, like = 0, dislike = 0, puntuation = 0)
                item.save()
                
        elif self.inEntry:
            if name == 'title':
                self.photo_name = self.content
            self.content = ""
            self.inContent = False

        elif not self.inEntry:
            if name == 'title':
                self.tag_name = str(self.content).split()[-1]
            self.content = ""
            self.inContent = False
    
    def characters (self, chars):
       if self.inContent:
            self.content = self.content + chars

# Load parser and driver
FlickrParser = make_parser()
FlickrParser.setContentHandler(FlickrHandler())

