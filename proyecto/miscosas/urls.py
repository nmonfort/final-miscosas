from django.urls import path
from . import views


urlpatterns = [
    path('main.css', views.css_view),
    path('rss', views.rss),
    path('login', views.login_view),
    path('logout', views.logout_view),
    path('registration', views.registration),
    path('alimentadores', views.alimentadores),
    path('usuarios', views.usuarios),
    path('usuarios/<str:user>', views.usuario),
    path('informacion', views.informacion),
    path('', views.main),
    path('youtube/<str:alimentador>', views.yt_alimentador),
    path('flickr/<str:alimentador>', views.flickr_alimentador),
    path('DGT/<str:alimentador>', views.DGT_alimentador),
    path('<str:item>', views.item_view),
]
